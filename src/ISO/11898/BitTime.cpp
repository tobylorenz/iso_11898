/*
 * Copyright (C) 2013-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "BitTime.h"

#include <cassert>
#include <cmath>

namespace ISO11898 {

BitTime::BitTime(uint8_t propSeg, uint8_t phaseSeg1, uint8_t phaseSeg2, uint8_t syncJumpWidth)
{
    /* check */
    assert((propSeg >= 1) && (propSeg <= 8));
    assert((phaseSeg1 >= 1) && (phaseSeg1 <= 8));
    assert((phaseSeg2 >= 1) && (phaseSeg2 <= 8));
    assert(phaseSeg1 >= phaseSeg2);
    assert((syncJumpWidth >= 1) && (syncJumpWidth <= 4));
    assert(syncJumpWidth <= phaseSeg1);

    /* set bit time */
    this->timeSeg1 = propSeg + phaseSeg1;
    this->timeSeg2 = phaseSeg2;
    this->syncJumpWidth = syncJumpWidth;
}

BitTime::BitTime(uint8_t timeSeg1, uint8_t timeSeg2, uint8_t syncJumpWidth)
{
    /* check */
    assert((timeSeg1 >= 2) && (timeSeg1 <= 16));
    assert((timeSeg2 >= 1) && (timeSeg2 <= 8));
    assert((timeSeg1 > timeSeg2)); // follows Phase_Seg2 = max(IPT, Phase_Seg1)
    assert((syncJumpWidth >= 1) && (syncJumpWidth <= 4));

    /* set bit time */
    this->timeSeg1 = timeSeg1;
    this->timeSeg2 = timeSeg2;
    this->syncJumpWidth = syncJumpWidth;
}

BitTime::~BitTime()
{
}

uint8_t BitTime::nominalBitTime() const
{
    return syncSeg + timeSeg1 + timeSeg2;
}

#if 0
void BitTime::setSamplePoint(double intendedSamplePoint)
{
    /* check */
    assert((intendedSamplePoint >= 0.0) && (intendedSamplePoint <= 1.0));

    /* remember the closest possible configuration */
    double closestSamplePoint = 0.0;
    uint8_t closestTimeSeg1 = 0;
    uint8_t closestTimeSeg2 = 0;
    uint8_t bestSyncJumpWidth = 0;
    double bestSyncJumpWidthPercent = 0.0;

    /* check for closest possible configuration */
    for (uint8_t propSeg = 1; propSeg <= 8; propSeg++) {
        for (uint8_t phaseSeg = 1; phaseSeg <= 8; phaseSeg++) {
            uint8_t currentTimeSeg1 = propSeg + phaseSeg;
            uint8_t currentTimeSeg2 = phaseSeg;
            uint8_t currentSyncJumpWidth = (4 < phaseSeg) ? 4 : phaseSeg;

            /* calculate sample point */
            double numerator =
                    static_cast<double>(syncSeg) +
                    static_cast<double>(currentTimeSeg1);
            double denominator =
                    numerator +
                    static_cast<double>(currentTimeSeg2);
            double currentSamplePoint = numerator / denominator;
            double currentSyncJumpWidthPercent = static_cast<double>(currentSyncJumpWidth) / denominator;

            /* check deviation from intended sample point */
            if (std::abs(currentSamplePoint - intendedSamplePoint) <
                    std::abs(closestSamplePoint - intendedSamplePoint)) {
                closestSamplePoint = currentSamplePoint;
                closestTimeSeg1 = currentTimeSeg1;
                closestTimeSeg2 = currentTimeSeg2;
                bestSyncJumpWidth = currentSyncJumpWidth;
                bestSyncJumpWidthPercent = currentSyncJumpWidthPercent;
            } else
            if ((std::abs(currentSamplePoint - intendedSamplePoint) ==
                    std::abs(closestSamplePoint - intendedSamplePoint)) &&
                    (currentSyncJumpWidthPercent > bestSyncJumpWidthPercent)) {
                closestSamplePoint = currentSamplePoint;
                closestTimeSeg1 = currentTimeSeg1;
                closestTimeSeg2 = currentTimeSeg2;
                bestSyncJumpWidth = currentSyncJumpWidth;
                bestSyncJumpWidthPercent = currentSyncJumpWidthPercent;
            }
            // @todo create a list and sort it, instead?
        }
    }

    /* set closest sample point */
    timeSeg1 = closestTimeSeg1;
    timeSeg2 = closestTimeSeg2;
    syncJumpWidth = bestSyncJumpWidth;
}
#endif

double BitTime::samplePoint() const
{
    return
            static_cast<double>(syncSeg + timeSeg1) /
            static_cast<double>(nominalBitTime());
}

double BitTime::maxOscillatorTolerance() const
{
    // note: both equations do static_cast<double> first as results may exceed uint8_t
    // note: equation 2 is actually min(phaseSeg1, phaseSeg2) / (2*(13*...-...))

    double equation1 =
            static_cast<double>(syncJumpWidth) /
            (20 * static_cast<double>(nominalBitTime()));

    double equation2 =
            static_cast<double>(timeSeg2) /
            (2 * (13 * static_cast<double>(nominalBitTime()) - static_cast<double>(timeSeg2)));

    return (equation1 < equation2) ? equation1 : equation2;
}

}
