/*
 * Copyright (C) 2013-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "../platform.h"

#include "../Controller.h"

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <linux/can.h>
#include <linux/can/error.h>
#include <linux/can/netlink.h>
#include <linux/can/raw.h>

#include <string>
#include <thread>
#include <vector>

namespace ISO11898 {
namespace Linux {

/** Linux CAN Controller */
class ISO_11898_EXPORT Controller : public ISO11898::Controller {
public:
    /**
     *
     * @param name Name (e.g. vcan0)
     * @param index Index
     */
    explicit Controller(std::string name = "vcan0", int index = 0);

    /* virtual methods */

    /** @todo virtual void setConfiguration(ISO11898::Configuration & newConfiguration) override; */
    /** @todo virtual ISO11898::Configuration getConfiguration() override; */
    virtual void setOnlineState(ISO11898::Controller::OnlineState newOnlineState) override;
    /** @todo virtual ISO11898::Controller::ErrorState getErrorState() override; */
    /** @todo virtual void getErrorCounts(uint8_t & transmit, uint8_t & receive) override; */
    /** @todo virtual ISO11898::Statistics getStatistics() override; */
    virtual void sendMessage(ISO11898::Message & message) override;

    static std::vector<std::string> probe();

private:
    /** socket */
    int sock;

    /** interface request */
    struct ifreq ifr;

    /** socket address */
    union {
        struct sockaddr addr;
        struct sockaddr_can addrCan;
    };

    /** receiver thread */
    std::thread * receiverThread;

    /**
     * receive message
     *
     * @param message received message
     * @return true on successful reception
     */
    bool receiveMessage(ISO11898::Message & message);

    /** receiver message thread */
    void receiveMessageThread();
};

}
}
