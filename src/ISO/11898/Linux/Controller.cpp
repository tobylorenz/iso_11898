/*
 * Copyright (C) 2013-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Controller.h"

#include <ifaddrs.h>
#include <unistd.h>
#include <sys/types.h>

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <string>

namespace ISO11898 {
namespace Linux {

Controller::Controller(std::string name, int index) :
    ISO11898::Controller(),
    sock(),
    ifr(),
    addrCan()
{
    /* interface request */
    strncpy(ifr.ifr_name, name.c_str(), sizeof(ifr.ifr_name));
    ifr.ifr_ifindex = index;

    /* socket address */
    addrCan.can_family = AF_CAN;
    addrCan.can_ifindex = ifr.ifr_ifindex;
}

void Controller::setOnlineState(ISO11898::Controller::OnlineState newOnlineState)
{
    /* check */
    if (m_onlineState == newOnlineState) {
        // no change
        return;
    }

    switch (newOnlineState) {
    case ISO11898::Controller::OnlineState::Online:
    {
        /* socket */
        sock = socket(PF_CAN, SOCK_RAW, CAN_RAW);
        if (sock < 0) {
            throw std::runtime_error("ISO11898::Linux::Controller::setOnlineState: Error while opening socket");
        }

        /* set socket timeout */
        timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = 1000; // 1 ms
        if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
            throw std::runtime_error("ISO11898::Linux::Controller::setOnlineState: Error setting timeout via setsockopt");
        }

        /* set filter to all */
        can_filter rfilter[1];
        rfilter[0].can_id = 0;
        rfilter[0].can_mask = 0;
        if (setsockopt(sock, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter)) < 0) {
            throw std::runtime_error("ISO11898::Linux::Controller::setOnlineState: Error setting filter via setsockopt");
        }

        /* set error filter to all */
        can_err_mask_t err_mask = CAN_ERR_MASK;
        if (setsockopt(sock, SOL_CAN_RAW, CAN_RAW_ERR_FILTER, &err_mask, sizeof(err_mask)) < 0) {
            throw std::runtime_error("ISO11898::Linux::Controller::setOnlineState: Error setting error filter via setsockopt");
        }

        /* set socket to loopback */
        int loopback = 1; // default
        if (setsockopt(sock, SOL_CAN_RAW, CAN_RAW_LOOPBACK, &loopback, sizeof(loopback)) < 0) {
            throw std::runtime_error("ISO11898::Linux::Controller::setOnlineState: Error setting CAN_RAW_LOOPBACK via setsockopt");
        }

        /* set socket to loopback */
        int recv_own_msgs = 0; // default
        if (setsockopt(sock, SOL_CAN_RAW, CAN_RAW_RECV_OWN_MSGS, &recv_own_msgs, sizeof(recv_own_msgs)) < 0) {
            throw std::runtime_error("ISO11898::Linux::Controller::setOnlineState: Error setting CAN_RAW_RECV_OWN_MSGS via setsockopt");
        }

        /* set socket to CANFD if available */
        if (setsockopt(sock, SOL_CAN_RAW, CAN_RAW_FD_FRAMES, nullptr, 0) == -ENOPROTOOPT) {
            // interface doesn't support CAN FD
            // @todo this is an error that can be handled
            throw std::runtime_error("ISO11898::Linux::Controller::setOnlineState: Device doesn't seem to support CAN FD");
        }

        // @todo CAN_RAW_JOIN_FILTERS

        /* ioctl */
        if (ioctl(sock, SIOCGIFINDEX, &ifr) < 0) {
            throw std::runtime_error("ISO11898::Linux::Controller::setOnlineState: Error while ioctl");
        }

        /* bind */
        if (bind(sock, &addr, sizeof(addrCan)) < 0) {
            throw std::runtime_error("ISO11898::Linux::Controller::setOnlineState: Error in socket bind");
        }

        /* start receive thread */
        receiverThread = new std::thread(&Controller::receiveMessageThread, this);
    }
        break;

    case ISO11898::Controller::OnlineState::Offline:
    case ISO11898::Controller::OnlineState::Listen: // no listen
    {
        /* shutdown socket */
        shutdown(sock, SHUT_RDWR);
        close(sock);
        sock = 0;

        /* stop receiver thread */
        receiverThread->join();
        delete receiverThread;
        receiverThread = nullptr;
    }
        break;
    }

    ISO11898::Controller::setOnlineState(newOnlineState);
}

void Controller::sendMessage(Message & message)
{
    /* check */
    assert(m_onlineState == ISO11898::Controller::OnlineState::Online);
    if (message.identifierExtension) {
        assert(message.identifier <= 0x3fff); // 29 bit
    } else {
        assert(message.identifier <= 0x7ff); // 11 bit
    }

    /* transfer structure */
    canfd_frame frame;
    frame.can_id = message.identifier;
    if (message.remoteTransmissionRequest) {
        frame.can_id |= CAN_RTR_FLAG;
    }
    if (message.identifierExtension) {
        frame.can_id |= CAN_EFF_FLAG;
    }
    frame.len = message.extendedDataLength ? message.dataLength() : message.dataLengthCode;
    frame.flags = 0;
    if (message.bitRateSwitch) {
        frame.flags |= CANFD_BRS;
    }
    if (message.errorStateIndicator) {
        frame.flags |= CANFD_ESI;
    }
    for (uint8_t i = 0; i < message.dataLength(); ++i) {
        frame.data[i] = message.data[i];
    }

    /* transmit */
    ssize_t nbytes = write(sock, &frame, message.extendedDataLength ? CANFD_MTU : CAN_MTU);
    if (nbytes < 0) {
        throw std::runtime_error("ISO11898::Linux::Controller::sendMessage: Error in write");
    }
}

bool Controller::receiveMessage(Message & message)
{
    /* receive */
    canfd_frame frame;
    ssize_t nbytes = read(sock, &frame, CANFD_MTU);
    if ((nbytes != CAN_MTU) && (nbytes != CANFD_MTU)) {
        return false;
    }

    /* transfer structure */
    message.identifier = frame.can_id & CAN_ERR_MASK;
    message.remoteTransmissionRequest = ((frame.can_id & CAN_RTR_FLAG) != 0);
    message.identifierExtension = ((frame.can_id & CAN_EFF_FLAG) != 0);
    message.extendedDataLength = (nbytes == CANFD_MTU);
    if (nbytes == CANFD_MTU) {
        message.bitRateSwitch = ((frame.flags & CANFD_BRS) != 0);
        message.errorStateIndicator = ((frame.flags & CANFD_ESI) != 0);
    }
    message.setDataLength(frame.len);
    for (uint8_t i = 0; i < message.dataLength(); ++i) {
        message.data.push_back(frame.data[i]);
    }

    /** get time stamp */
    timeval tv;
    ioctl(sock, SIOCGSTAMP, &tv);
    // tv.tv_sec = 0;
    // tv.tv_usec = 0;
    // @todo get time stamp

    return true;
}

void Controller::receiveMessageThread()
{
    while (sock) {
        Message message;
        if (receiveMessage(message)) {
            messageReceived.emit(message);
        }
    }
}

std::vector<std::string> Controller::probe()
{
    std::vector<std::string> controllerNames;

    /* get interfaces addresses */
    ifaddrs *addrs;
    if (getifaddrs(&addrs) < 0) {
        // error occurred
        return controllerNames;
    }

    /* loop through them */
    ifaddrs * tmp = addrs;
    while (tmp) {
        /* check if it's CAN */
        if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_CAN) {
            controllerNames.push_back(tmp->ifa_name);
        }

        /* next */
        tmp = tmp->ifa_next;
    }

    /* free the list */
    freeifaddrs(addrs);

    return controllerNames;
}

}
}
