/*
 * Copyright (C) 2013-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Controller.h"

namespace ISO11898 {

Controller::Controller() :
    onlineStateChanged(),
    errorStateChanged(),
    messageReceived(),
    m_configuration(),
    m_onlineState(OnlineState::Offline),
    m_errorState(ErrorState::ErrorActive),
    m_statistics()
{
}

Controller::Controller(const Controller & rhs) :
    onlineStateChanged(),
    errorStateChanged(),
    messageReceived()
{
    operator=(rhs);
}

Controller::~Controller()
{
    setOnlineState(OnlineState::Offline);
}

Controller & Controller::operator=(const Controller & rhs)
{
    if (this != &rhs) {
        m_configuration = rhs.m_configuration;
        m_onlineState = rhs.m_onlineState;
        m_errorState = rhs.m_errorState;
        m_statistics = rhs.m_statistics;
    }

    return *this;
}

void Controller::setConfiguration(Configuration & newConfiguration)
{
    m_configuration = newConfiguration;
}

Configuration Controller::configuration() const
{
    return m_configuration;
}

void Controller::setOnlineState(OnlineState /*newOnlineState*/)
{
}

Controller::OnlineState Controller::onlineState() const
{
    return m_onlineState;
}

Controller::ErrorState Controller::errorState() const
{
    return m_errorState;
}

void Controller::errorCounts(uint8_t & transmit, uint8_t & receive)
{
    transmit = 0;
    receive = 0;
}

Statistics Controller::statistics() const
{
    return m_statistics;
}

void Controller::sendMessage(Message & /*message*/)
{
}

}
