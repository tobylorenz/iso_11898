# dependencies
include(GenerateExportHeader)
include(FindPkgConfig)
pkg_check_modules(SIGC++ REQUIRED sigc++-2.0>=2.5.1)

# search paths
include_directories(
    ${CMAKE_CURRENT_BINARY_DIR}
    ${SIGC++_INCLUDE_DIRS})
link_libraries(
    ${SIGC++_LIBRARY_DIRS})

# sources/headers
set(source_files
    BitTime.cpp
    Configuration.cpp
    Controller.cpp
    Message.cpp
    Statistics.cpp)
set(public_header_files
    BitTime.h
    Configuration.h
    Controller.h
    Message.h
    platform.h
    Statistics.h)
set(private_header_files
    )

# generated files
configure_file(config.h.in config.h)
configure_file(${PROJECT_NAME}.pc.in ${PROJECT_NAME}.pc @ONLY)

# compiler/linker settings
if(CMAKE_COMPILER_IS_GNUCXX)
    add_definitions(-std=c++11)
endif()

# targets
add_library(${PROJECT_NAME} SHARED
    ${source_files} ${public_header_files} ${private_header_files})
generate_export_header(${PROJECT_NAME})
set_target_properties(${PROJECT_NAME} PROPERTIES
    VERSION ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}
    SOVERSION ${PROJECT_VERSION_MAJOR}
    CXX_VISIBILITY_PRESET "hidden"
    VISIBILITY_INLINES_HIDDEN 1)
target_link_libraries(${PROJECT_NAME}
    ${SIGC++_LIBRARIES})

# install
install(
    TARGETS ${PROJECT_NAME}
    DESTINATION ${CMAKE_INSTALL_LIBDIR})
install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.pc
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig)
install(
    FILES
        ${CMAKE_CURRENT_BINARY_DIR}/iso_11898_export.h
        ${CMAKE_CURRENT_BINARY_DIR}/config.h
        ${public_header_files}
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/ISO/11898)

# sub directories
message("Compiling drivers for ${CMAKE_SYSTEM_NAME}")
if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    add_subdirectory(Linux)
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
    add_subdirectory(8devices_CANAL)
    add_subdirectory(ESD_NTCAN)
    add_subdirectory(ETAS_BOA)
    add_subdirectory(ICS_neoVI)
    add_subdirectory(ISO22900)
    add_subdirectory(Ixxat_ECI)
    add_subdirectory(Ixxat_VCI)
    add_subdirectory(Kvaser_CAN)
    add_subdirectory(MHS_TinyCAN)
    add_subdirectory(NI_CAN)
    add_subdirectory(NI_DNET)
    add_subdirectory(NI_XNET)
    add_subdirectory(PEAK_CAN)
    add_subdirectory(PEAK_PCAN_Basic)
    add_subdirectory(PEAK_PCAN_Light)
    add_subdirectory(RP1210)
    add_subdirectory(SAE_J2534)
    add_subdirectory(Softing_CAN)
    add_subdirectory(Sontheim_MT)
    add_subdirectory(Vector_XL)
    add_subdirectory(VS_CAN)
endif()
add_subdirectory(docs)
add_subdirectory(tests)
