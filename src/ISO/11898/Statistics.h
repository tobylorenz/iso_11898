/*
 * Copyright (C) 2013-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <cstdint>

#include "iso_11898_export.h"

namespace ISO11898 {

/** CAN Statistics */
class ISO_11898_EXPORT Statistics
{
public:
    Statistics();
    virtual ~Statistics();

    /** Standard Data Frame Count */
    uint32_t standardDataFrameCount;

    /** Standard Remote Frame Count */
    uint32_t standardRemoteFrameCount;

    /** Extended Data Frame Count */
    uint32_t extendedDataFrameCount;

    /** Extended Remote Frame Count */
    uint32_t extendedRemoteFrameCount;

    /** Error Frame Count */
    uint32_t errorFrameCount;

    /** Overload Frame Count */
    uint32_t overloadFrameCount;

    /** Bus Load */
    double busLoad; // 0.0 .. 1.0
};

}
