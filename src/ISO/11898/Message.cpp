/*
 * Copyright (C) 2013-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Message.h"

#include <cassert>

namespace ISO11898 {

Message::Message() :
    identifier(0),
    remoteTransmissionRequest(false),
    identifierExtension(false),
    extendedDataLength(false),
    bitRateSwitch(false),
    errorStateIndicator(false),
    dataLengthCode(0),
    data()
{
}

bool Message::operator==(const Message & rhs)
{
    return
        (identifier == rhs.identifier) &&
        (remoteTransmissionRequest == rhs.remoteTransmissionRequest) &&
        (identifierExtension == rhs.identifierExtension) &&
        (extendedDataLength == rhs.extendedDataLength) &&
        (bitRateSwitch == rhs.bitRateSwitch) &&
        (errorStateIndicator == rhs.errorStateIndicator) &&
        (dataLengthCode == rhs.dataLengthCode) &&
        (data == rhs.data);
}

bool Message::operator!=(const Message & rhs)
{
    return !operator==(rhs);
}

uint8_t Message::dataLength() const
{
    /* CAN and CAN FD are identical with DLC<=8 */
    if (dataLengthCode <= 8) {
        return dataLengthCode;
    }

    /* CAN is limited to 8 data bytes */
    if (extendedDataLength == false) {
        return 8;
    }

    /* CAN FD handles DLC>8 differently */
    switch (dataLengthCode) {
    case 9:
        return 12;
    case 10:
        return 16;
    case 11:
        return 20;
    case 12:
        return 24;
    case 13:
        return 32;
    case 14:
        return 48;
    case 15:
        return 64;
    }

    /* fail safe: 0 byte */
    return 0;
}

uint8_t Message::setDataLength(uint8_t dataLength)
{
    /* check */
    if (extendedDataLength) {
        assert(dataLength <= 64);
    } else {
        assert(dataLength <= 8);
    }

    /* CAN and CAN FD are identical with DLC<=8 */
    if (dataLength <= 8) {
        dataLengthCode = dataLength;
        return dataLengthCode;
    }

    /* CAN is limited to 8 data bytes */
    if (extendedDataLength == false) {
        dataLengthCode = 8;
        return dataLengthCode;
    }

    /* CAN FD handles DLC>8 differently */
    if (dataLength <= 12) {
        dataLengthCode = 9;
        return dataLengthCode;
    }
    if (dataLength <= 16) {
        dataLengthCode = 10;
        return dataLengthCode;
    }
    if (dataLength <= 20) {
        dataLengthCode = 11;
        return dataLengthCode;
    }
    if (dataLength <= 24) {
        dataLengthCode = 12;
        return dataLengthCode;
    }
    if (dataLength <= 32) {
        dataLengthCode = 13;
        return dataLengthCode;
    }
    if (dataLength <= 48) {
        dataLengthCode = 14;
        return dataLengthCode;
    }

    /* fail safe: 64 byte */
    dataLengthCode = 15;
    return dataLengthCode;
}

}
