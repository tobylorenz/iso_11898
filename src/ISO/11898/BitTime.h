/*
 * Copyright (C) 2013-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <cstdint>

#include "iso_11898_export.h"

namespace ISO11898 {

/**
 * Bit Time
 *
 * The Bit time consists of
 *   - SyncSeg: 1 tq
 *   - PropSeg: 1..8 tq
 *   - PhaseSeg1: 1..8 tq
 *   - PhaseSeg2 = max(IPT, PhaseSeg1): 1..8 tq
 * Note: This leads to PhaseSeg2 <= PhaseSeg1.
 * Note: This leads to 288 valid combinations from 512 total combinations.
 *
 * Combined they are
 *   - TimeSeg1 = PropSeg + PhaseSeg1: 2..16 tq
 *   - TimeSeg2 = PhaseSeg2: 1..8 tq
 *   - NominalBitTime = SyncSeg + PropSeg + PhaseSeg1 + PhaseSeg2: 4..25 tq
 * Note: This lead to TimeSeg2 < TimeSeg1.
 *
 * Synchronization is done with
 *   - ReSync Jump Width: 1..min(4, PhaseSeg1)
 *
 * TimeQuantum [s] = BaudRatePrescaler / OscillatorFrequency [Hz]
 * BitRate [Bit/s] = 1 / (NominalBitTime [tq/bit] * TimeQuantum [s/tq])
 */
class ISO_11898_EXPORT BitTime {
public:
    /**
     * Set bit time.
     *
     * @param propSeg Propagation Segment
     * @param phaseSeg1 Phase Segment 1
     * @param phaseSeg2 Phase Segment 2
     * @param syncJumpWidth Re-Synchronization Jump Width
     */
    explicit BitTime(uint8_t propSeg, uint8_t phaseSeg1, uint8_t phaseSeg2, uint8_t syncJumpWidth);

    /**
     * Set bit time.
     *
     * @param timeSeg1 Time Segment 1 (=Propagation Segment + Phase Segment 1)
     * @param timeSeg2 Time Segment 2 (=Phase Segment 2)
     * @param syncJumpWidth Re-Synchronization Jump Width
     */
    explicit BitTime(uint8_t timeSeg1, uint8_t timeSeg2, uint8_t syncJumpWidth);

#if 0
    /**
     * Set bit time.
     *
     * @param samplePoint
     */
    explicit BitTime(double samplePoint);
#endif

    virtual ~BitTime();

    /** Synchronization Segment */
    static constexpr uint8_t syncSeg = 1;

    /** Time Segment 1 (Propagation Segment + Phase Segment 1) */
    uint8_t timeSeg1 : 5; // 2..16

    /** Time Segment 2 (Phase Segment 2) */
    uint8_t timeSeg2 : 4; // 1..8

    /** Sync Jump Width */
    uint8_t syncJumpWidth : 3; // 1..4

    /**
     * Returns nominal bit time (NBT).
     *
     * @return nominal bit time (NBT)
     */
    uint8_t nominalBitTime() const; // 4..25

#if 0
    /**
     * Set sample point.
     *
     * It is possible that the intended sample point is not exactly
     * possible with the configuration possibilities.
     * Then the closest possible sample point is set.
     *
     * @param[in] intendedSamplePoint Sample Point (55.55% .. 94.44%)
     */
    void setSamplePoint(double intendedSamplePoint);
#endif

    /**
     * Returns the sample point.
     *
     * @return sample point
     */
    double samplePoint() const; // 55.55% .. 94.44%

    /**
     * Returns the maximum oscillator tolerance.
     *
     * @return maximum oscillator tolerance
     */
    double maxOscillatorTolerance() const; // 0.21% .. 1.66% (both assuming max SJW)
};

}
