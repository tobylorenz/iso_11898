/*
 * Copyright (C) 2013-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <cstdint>
#include <vector>

#include "iso_11898_export.h"

namespace ISO11898 {

/** CAN Message */
class ISO_11898_EXPORT Message
{
public:
    Message();

    bool operator==(const Message & rhs);
    bool operator!=(const Message & rhs);

    /** standard 11 bit (IDE=false) or extended 29 bit identifier (IDE=true) */
    uint32_t identifier : 29;

    /** remote transmission request (RTR) / substitute remote request (SRR) */
    bool remoteTransmissionRequest : 1;

    /** identifier extension (IDE) */
    bool identifierExtension : 1;

    /** extended data length (EDL) */
    bool extendedDataLength : 1;

    /** bit rate switch (BRS) */
    bool bitRateSwitch : 1;

    /** error state indicator (ESI) */
    bool errorStateIndicator : 1;

    /** data length code (DLC) */
    uint8_t dataLengthCode : 4;

    /**
     * data length calculated from DLC
     *
     * @return data length
     */
    uint8_t dataLength() const;

    /**
     * set DLC to accommodate given data length
     *
     * @param[in] dataLength data langth
     * @return DLC
     */
    uint8_t setDataLength(uint8_t dataLength);

    /** data */
    std::vector<std::uint8_t> data;
};

}
