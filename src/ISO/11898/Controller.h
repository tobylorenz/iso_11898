/*
 * Copyright (C) 2013-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <sigc++-2.0/sigc++/signal.h>

#include "Configuration.h"
#include "Message.h"
#include "Statistics.h"

#include "iso_11898_export.h"

namespace ISO11898 {

/** CAN Controller */
class ISO_11898_EXPORT Controller {
public:
    Controller();
    Controller(const Controller & rhs);
    Controller & operator=(const Controller & rhs);
    virtual ~Controller();

    /**
     * set configuration
     *
     * @param[in] newConfiguration configuration
     */
    virtual void setConfiguration(Configuration & newConfiguration);

    /**
     * get configuration
     *
     * @return configuration
     */
    virtual Configuration configuration() const;

    /** online state */
    enum class OnlineState : uint8_t {
        /** offline */
        Offline,

        /** online */
        Online,

        /** listen (no acknowledge) */
        Listen
    };

    /**
     * set online state
     *
     * @param[in] newOnlineState online state
     */
    virtual void setOnlineState(OnlineState newOnlineState);

    /**
     * get online state
     *
     * @return online state
     */
    virtual OnlineState onlineState() const;

    /** online state changed */
    sigc::signal<void, OnlineState> onlineStateChanged;

    /** error state */
    enum class ErrorState : uint8_t {
        /** error active */
        ErrorActive,

        /** error passive */
        ErrorPassive,

        /** bus off */
        BusOff
    };

    /**
     * get error state
     *
     * @return error state
     */
    virtual ErrorState errorState() const;

    /** error state changed */
    sigc::signal<void, ErrorState> errorStateChanged;

    /**
     * get error counts
     *
     * @param[out] transmit error counter
     * @param[out] receive error counter
     */
    virtual void errorCounts(uint8_t & transmit, uint8_t & receive);

    /**
     * get statistics
     *
     * @return statistics
     */
    virtual Statistics statistics() const;

    /**
     * send message
     *
     * @param[in] message message
     */
    virtual void sendMessage(Message & message);

    /** receive message */
    sigc::signal<void, Message> messageReceived; // @todo std::chrono time_point is missing

protected:
    /** configuration */
    Configuration m_configuration;

    /** online state */
    OnlineState m_onlineState;

    /** error state */
    ErrorState m_errorState;

    /** statistics */
    Statistics m_statistics;
};

}
